#! /usr/bin/python

import os
import re
import sys
import zipfile

EXIT_ON_FIRST_ERROR = False # or collect all errors?

errorFound = False


def error(arg):
    if EXIT_ON_FIRST_ERROR:
        exit("    ERROR: " + arg)
    else:
        print("    ERROR: " + arg)
        global errorFound
        errorFound = True


def checkZipfile(zipfilename):
    assert os.path.isfile(zipfilename)
    try:
        zip = zipfile.ZipFile(zipfilename)
    except zipfile.BadZipfile:
        error("zipfile is invalid")
        return
    namelist = zip.namelist()
    if not namelist:
        error("zipfile is empty")
        return

    # check whether there is a single root directory for all files.
    rootDirectory = namelist[0].split('/')[0] + '/'
    for name in namelist:
        if not name.startswith(rootDirectory):
            error("file '{}' is not located under a common root directory".format(name))

    # check whether there is a license.
    pattern = re.compile(rootDirectory + '(License|LICENSE).*');
    if not any(pattern.match(name) for name in namelist):
        error("no license file found")

    # check whether there are unwanted files
    pattern = re.compile('.*(\/\.git|\/\.svn|\/__MACOSX|\/\.aptrelease).*');
    for name in namelist:
        if pattern.match(name):
            error("file '{}' should not be part of the zipfile".format(name))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        error("one parameter as directory name expected, but got '{}'".format(sys.argv))
        exit(errorFound)
    directory = sys.argv[1]
    if not os.path.isdir(directory):
        error("directory '{}' not found".format(directory))
        exit(errorFound)

    # check each file in the directory
    for filename in sorted(os.listdir(directory)):
        fullname = os.path.join(directory, filename)
        print("CHECKING '{}'".format(filename))
        if not os.path.isfile(fullname):
            error("unexpected file or directory '{}'".format(fullname))
        elif filename.endswith(".zip"):
            checkZipfile(fullname)
        elif not filename == "README.md":
            error("unexpected file or directory '{}'".format(fullname))

    exit(errorFound)
